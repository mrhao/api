package com.api.controller;
import org.springframework.web.bind.annotation.RestController;

import com.api.TenantContext;
import com.api.exception.ResourceNotFoundException;
import com.api.model.Server;
import com.api.model.ServerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

//import com.api.Server;

//@Api(name = "ServerApi", description = "Server API Service")
@RestController
@RequestMapping("/api")
public class ServerController {

	@Autowired
    ServerRepository serverRepository;
    
    @PostMapping("/server/{serverId}")
    public Server createServer(@PathVariable(value = "serverId") Integer serverId, @Valid @RequestBody Server server) {
    	String serverPrefix = "server";
    	String tenantName = serverPrefix.concat(serverId.toString());    	
    	TenantContext.setCurrentTenant(tenantName);
    	
        return serverRepository.save(server);
    }
    
    @GetMapping("/server/{serverId}")
    public List<Server> getAllServer(@PathVariable(value = "serverId") Integer serverId) {
    	String serverPrefix = "server";
    	String tenantName = serverPrefix.concat(serverId.toString());    	
    	TenantContext.setCurrentTenant(tenantName);
    	
        return serverRepository.findAll();
    }

    //@Autowired
	//private ServerService serverService;
    
    @GetMapping("/server/{serverId}/id/{id}")
    public Server getServerById(@PathVariable(value = "serverId") Integer serverId, @PathVariable(value = "id") Integer id) {
    	
    	String serverPrefix = "server";
    	String tenantName = serverPrefix.concat(serverId.toString());    	
    	TenantContext.setCurrentTenant(tenantName);    	
    	//Object currentTenant = TenantContext.getCurrentTenant();
    	//System.out.println("Current: " + currentTenant);
    	
        return serverRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("Server", "id", id));
    }

}
